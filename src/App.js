import React, { Component } from 'react';
import { Login,Main,Navbar } from "./components/template"

export default class App extends Component {

  static defaultProps = { 
    dummy: []       
}

  constructor(props) {
    super(props);
    this.state = {
      dummy: [
        {
          "username" : "admin",
          "password" : "admin",
          "role" : "admin"
        },
        {
          "username" : "namapedagang",
          "password" : "passwordpedagang",
          "role" : "pedagang"
        },
        {
          "username" : "user",
          "password" : "user",
          "role" : "user"
        },
        {
          "username" : "samplepedagang",
          "password" : "samplepedagang",
          "role" : "pedagang"
        }],
      current: {
        "username" : "",
        "password": "",
        "role" : "",
        "status" : false
      }
    }
  }

  newPedagang = (username, password) => {
    this.state.dummy.push({
      "username" : username,
      "password" : password,
      "role" : "pedagang"
    })
  }

  setCurrent = (username, password, role) => {
    this.setState({ current: {
      "username" : username,
      "password" : password,
      "role" : role,
      "status" : true
    } })
  }

  logout = () => {
    this.setState({ current: {
      "status" : false
    }})
  }

  render() {
    const login = this.state.current.status === false ? <Login setCurrent={this.setCurrent} dummy={this.state.dummy} current={this.state.current}/> : <Main deletePedagang={this.deletePedagang} newPedagang={this.newPedagang} current={this.state.current} dummy={this.state.dummy} />
    const navbar = this.state.current.status === true ? <Navbar logout={this.logout} /> : ""

    return (
      <div>
        {navbar}
        {login}
      </div>
    );
  }
}