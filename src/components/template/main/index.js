import React, { Component } from 'react'
import './index.css'
import Admin from '../admin'
import Pedagang from '../pedagang'
import User from '../user'

export default class Main extends Component {
    
    constructor(props)
    {
        super(props);
    }
    
    render() {
        const admin = this.props.current.role === "admin" ? < Admin deletePedagang={this.props.deletePedagang} newPedagang={this.props.newPedagang} current={this.props.current} dummy={this.props.dummy} /> : ""
        const pedagang = this.props.current.role === "pedagang" ? < Pedagang current={this.props.current} /> : ""
        const user = this.props.current.role === "user" ? < User current={this.props.current} /> : ""
        return (
            <div>
                {admin}
                {pedagang}
                {user}
            </div>
        )
    }
}
