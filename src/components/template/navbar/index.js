import React, { Component } from 'react'
import './index.css'

export default class Navbar extends Component {
    
    constructor(props)
    {
        super(props);
    }

    logout = () => {
        this.props.logout()
    }
    
    render() {
        return (
            <div>
                <nav>
                <a>Home</a>
                <a onClick={this.logout}>Logout</a>
                <div class="animation start-home"></div>
                </nav>
            </div>
        )
    }
}
