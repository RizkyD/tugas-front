import React, { Component } from 'react'
import './index.css'
import Update from "../update"

export default class Admin extends Component {
    
    constructor(props)
    {
        super(props);
        this.state = {
            update: false,
            index: null,
            username: null
        }
    }

    setIndex = (index, username) => {
        this.setState({
            index : index,
            update : true,
            username: username
        })
    }

    register = (e) => {
        e.preventDefault();

        const username = e.target.username.value;
        const password = e.target.password.value;

        this.props.newPedagang(username,password);
        this.forceUpdate();
    }


    delete = (index) => {
        this.props.dummy.filter(account => account.role === "pedagang")
        this.props.dummy.splice(index,1)
        console.log(this.props.dummy);
        this.forceUpdate()
    }

    update = (e, index) => {
        e.preventDefault();

        const uusername = e.target.updateusername.value;
        const upasword = e.target.updatepassword.value;

        this.props.dummy.filter(account => account.role === "pedagang")
        this.props.dummy.splice(index,1, {
            "username" : uusername,
            "password" : upasword,
            "role" : "pedagang"
        })
    }
    
    render() {
        const list = this.props.dummy.filter(account => account.role === "pedagang").map((acc, index) =>
                <tr>
                    <td>{acc.username}</td>
                    <td>{acc.password}</td>
                    <td>{acc.role}</td>
                    <td><button onClick={() => this.setIndex(index, acc.username)} >edit</button></td>
                    <td><button onClick={() => this.delete(index)} >Delete</button>{index}</td>
                </tr>
                );
        const view = this.state.update === true ? <Update username={this.state.username} index={this.state.index} dummy={this.props.dummy}/> : ""
        return (
            <div>
                <table>
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Role</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {list}
                        </tbody>
                        
                </table>

                <br/>
                <div>Tambah data {}</div>
                <form onSubmit={this.register}>
                    <input class="usernamee" name="username" type="text" align="center" placeholder="Username" ></input>
                    <input class="passworde" name="password" type="password" align="center" placeholder="Password"></input>
                    <button type="submit" class="submite" align="center">tambah pedagang</button>
                </form>
                <br/>
                <br/>
                {view}
            </div>
        )
    }
}
