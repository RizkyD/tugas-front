import React, { Component } from 'react'
import './index.css'
import axios from 'axios'

export default class User extends Component {
    
    constructor(props)
    {
        super(props);
        this.state= {
            json : [],
            perPage: 10,
            currentPage: 1,
            pageMax: 5,
            pageCount: 500,
        }
    }

    componentDidMount() {
        this.axios();
    }

    axios() {
        axios
        .get(`https://jsonplaceholder.typicode.com/photos?_limit=${this.state.perPage}&_start=${this.state.currentPage}`)
        .then((res) => {
          this.setState({
            json: res.data,
          });
        })
        .catch((e) => console.log(e));
    }

    componentDidUpdate(props, state) {
        if (state.json !== this.state.json) this.axios();
    }

    page(value) {
        this.setState({
            currentPage: value,
          });
    }

    first() {
        this.setState({
            currentPage: 1
        })
    }

    last() {
        this.setState({
            currentPage: 500
        })
    }
    
    render() {
        const first = this.state.currentPage !== 1 ? <button onClick={() => this.first()}>first</button> : ""
        const last = this.state.currentPage !== 500 ? <button onClick={() => this.last()}>last</button> : ""

        const pagelength = [];

        let start = null;
        if (this.state.currentPage < 5) {
        start = this.state.currentPage;
        } else if (this.state.currentPage > 498) {
        start = 496;
        } else {
        start = this.state.currentPage - 2;
        }

        for (let i = start; i < start + 5; i++) {
        if (this.state.currentPage <= this.state.pageCount) {
            pagelength.push(i);
        }}

        return (
            <div class="exceed">
                {this.state.json.map((value) => {
          return (
                <div>
                    <img src={value.thumbnailUrl} alt="" />
                    <span>Title : {value.title} </span>
                </div>
                    );
                })}
                <div class="new">
                    <div>
                        {first}
                        {pagelength.map((value) => {
                        return (
                            <div>
                                <button onClick={() => this.page(value)} class={this.state.currentPage === value ? "active" : ""}>
                                    {value}
                                </button>
                            </div>
                            );
                        })}
                        {last}
                    </div>
                </div>
            </div>
        )
    }
}
