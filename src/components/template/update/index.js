import React, { Component } from 'react'
import './index.css'

export default class Update extends Component {
    
    constructor(props)
    {
        super(props);
    }

    update = (e) => {
        e.preventDefault();

        const uusername = e.target.updateusername.value;
        const upasword = e.target.updatepassword.value;

        const newData = this.props.dummy.findIndex((v) => {
            return v.username === this.props.username
        })

        this.props.dummy.filter(account => account.role === "pedagang")
        this.props.dummy.splice(newData,1, {
            "username" : uusername,
            "password" : upasword,
            "role" : "pedagang"
        })
        this.forceUpdate();
    }
    
    render() {
        return (
            <div>
                <div>Update data {}</div>
                <form class="form" onSubmit={this.update}>
                <input name="updateusername" type="text" align="center" placeholder="Username" ></input>
                <input name="updatepassword" type="password" align="center" placeholder="Password"></input>
                <button type="submit" align="center" onClick={this.props.refreshken}>update</button>
                </form>
            </div>
        )
    }
}
